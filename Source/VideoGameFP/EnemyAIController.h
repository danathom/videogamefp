// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "VideoGameFPHUD.h"
#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class VIDEOGAMEFP_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()
	
public:
    enum State {
        Start,
        Chase,
        Attack,
        Frozen
    };
    
    virtual void BeginPlay() override;
    virtual void Tick( float DeltaSeconds ) override;
    virtual void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;
    
private:
    AVideoGameFPHUD *HUD;
    APawn* playerPawn;
    class AEnemy* thisEnemy;
    State currentState;
    bool bearWarningShown = false;
    FTimerHandle BearTimer;
    
    UPROPERTY(EditAnywhere, Category = Weapon)
    float range = 150.0f;
    float warningRange = 600.0f;

    
    void ChasePlayer ();
	
};
