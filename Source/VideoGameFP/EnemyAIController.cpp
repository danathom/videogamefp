// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "Enemy.h"
#include "EnemyAIController.h"

void AEnemyAIController::BeginPlay() {
    Super::BeginPlay();
    
    playerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
    currentState = Start;
    APawn* pawn = GetPawn();
    thisEnemy = Cast<AEnemy>(GetPawn());
    HUD = Cast<AVideoGameFPHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
}

void AEnemyAIController::Tick(float deltaTime)
{
    Super::Tick(deltaTime);
    switch (currentState) {
        case Start:
            ChasePlayer();
            break;
        case Attack:
            playerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
            if (playerPawn == nullptr) return;
            FVector playerPos = playerPawn->GetActorLocation();
            thisEnemy = Cast<AEnemy>(GetPawn());
            if (thisEnemy == nullptr) return;
            FVector enemyPos = thisEnemy->GetActorLocation();
            if (FVector::Dist(playerPos, enemyPos) > range) {
                thisEnemy->StopAttack();
                ChasePlayer();
            }
            break;
    }
    playerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
    if (playerPawn == nullptr) return;
    FVector playerPos = playerPawn->GetActorLocation();
    thisEnemy = Cast<AEnemy>(GetPawn());
    if (thisEnemy == nullptr) return;
    FVector enemyPos = thisEnemy->GetActorLocation();
    if (!bearWarningShown && FVector::Dist(playerPos, enemyPos) < warningRange) {
        HUD->ToggleBearText();
        bearWarningShown = true;
        thisEnemy->startBearWarningDissappearTimer();
    }
    
    if (thisEnemy->frozen) {
        thisEnemy->frozen = false;
        GetWorldTimerManager().SetTimer(BearTimer, this, &AEnemyAIController::ChasePlayer, 10.0f, false);
    }
}

void AEnemyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
    if (!Result.IsSuccess()) return;
    
    playerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
    if (playerPawn == nullptr) return;
    FVector playerPos = playerPawn->GetActorLocation();
    thisEnemy = Cast<AEnemy>(GetPawn());
    if (thisEnemy == nullptr) return;
    FVector enemyPos = thisEnemy->GetActorLocation();
    if (FVector::Dist(playerPos, enemyPos) > range) {
        return;
    }
    
    if (thisEnemy == nullptr) {
            thisEnemy = Cast<AEnemy>(GetPawn());
        if (thisEnemy == nullptr) return;
    }
    currentState = Attack;
    thisEnemy->StopChase();
    thisEnemy->StartAttack();
}

void AEnemyAIController::ChasePlayer ()
{
    currentState = Chase;
    playerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
    if (playerPawn == nullptr) return;
    MoveToActor (playerPawn);
    thisEnemy = Cast<AEnemy>(GetPawn());
    if (thisEnemy == nullptr) return;
    thisEnemy->StartChase();
}

