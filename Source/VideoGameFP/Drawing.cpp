// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "Drawing.h"


// Sets default values
ADrawing::ADrawing()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADrawing::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADrawing::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

