// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "SpawnManager.generated.h"

UCLASS()
class VIDEOGAMEFP_API ASpawnManager : public AActor
{
    GENERATED_BODY()
    
public:
    // Sets default values for this actor's properties
    ASpawnManager();
    
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    
    // Called every frame
    virtual void Tick( float DeltaSeconds ) override;
    
    UPROPERTY(EditAnywhere, Category = Spawn)
    TArray<class ATargetPoint*> SpawnPoints;
    
    UPROPERTY(EditAnywhere, Category = Spawn)
    TSubclassOf<ACharacter> CharacterClass;
    
    UPROPERTY(EditAnywhere, Category = Spawn)
    float SpawnTime = 25.0f;
    
    int spawnMax = 1; //TODO: change to int based on current level (ex. currentLevel *3 )
    int numSpawns = 0;
    
    void SpawnBear();
private:
    FTimerHandle SpawnTimer;
    void OnSpawnTimer();
};
