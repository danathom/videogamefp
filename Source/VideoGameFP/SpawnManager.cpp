// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "Engine/TargetPoint.h"
#include "SpawnManager.h"
#include <iostream>


// Sets default values
ASpawnManager::ASpawnManager()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    
}

// Called when the game starts or when spawned
void ASpawnManager::BeginPlay()
{
    Super::BeginPlay();
}

// Called every frame
void ASpawnManager::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );
    
}

void ASpawnManager::OnSpawnTimer()
{
    int totalSpawns = SpawnPoints.Num();
    if (totalSpawns == 0 || numSpawns >= spawnMax) return;
    
    int index = FMath::RandRange(0,totalSpawns-1);
    ATargetPoint* spawned = SpawnPoints[index];
    FVector spawnLocation = spawned->GetActorLocation();
    FRotator spawnRotation = spawned->GetActorRotation();
    
    // Spawn an ACharacter of subclass CharacterClass, at specified position and rotation
    ACharacter* Char = GetWorld()->SpawnActor<ACharacter>(CharacterClass, spawnLocation, spawnRotation);
    if (!Char) return;
    
    // Spawn the AI controller for the character
    Char->SpawnDefaultController();
    std::cout << "Spawning bear" << std::endl;
    numSpawns++;
}

void ASpawnManager::SpawnBear() {
    GetWorldTimerManager().SetTimer(SpawnTimer, this, &ASpawnManager::OnSpawnTimer, 1.0f, true);
}
