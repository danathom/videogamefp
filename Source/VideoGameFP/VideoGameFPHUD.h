// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once 
#include "GameFramework/HUD.h"
#include "VideoGameFPHUD.generated.h"

UCLASS()
class AVideoGameFPHUD : public AHUD
{
	GENERATED_BODY()

public:
	AVideoGameFPHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;
    virtual void BeginPlay() override;
    
    void SetInstructionText(FString Text);
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Font)
    UFont* GameFont;
    
    void ToggleBearText();
    void SetTimerText(FString timeText) { TimerText = timeText; }
    void SetTimerPercent(float timePercent) {TimerPercent = timePercent; }
    void SetDamagedMessage(FString damageText);
    void ClearDamagedMessage();
    void SetEndGameText(FString text);
    void HandleGameOver();
    void DisplayTimeLost();
    void HideTimeLost();
    void TimerOver();
    
    void SetBearFrozenPercent(float freezePercent) {FrozenPercent = freezePercent; }
    
    void UpdatePaintedObjectsCount();
private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;
    
    FLinearColor GameFontColor;
    
    FString InstructionText;
    FString BearText;
    FString TimerText;
    FString TimeLostText;
    FString DamagedMessage;
    FString ObjectsRemainingText;
    FString EndGameText;
    
    FColor TimerTextColor;
    
    
    bool IsGameOver;
    
    float ObjectsRemainingPercent;
    float TimerPercent;
    float FrozenPercent;
};

