// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "VideoGameFP.h"
#include "VideoGameFPHUD.h"
#include "Engine/Canvas.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "VideoGameFPGameMode.h"
#include <iostream>

AVideoGameFPHUD::AVideoGameFPHUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshiarTexObj(TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
	CrosshairTex = CrosshiarTexObj.Object;
    
    GameFontColor = FLinearColor(1, 1, 1, 1);
    
    /*ConstructorHelpers::FObjectFinder<UFont> FontObject(TEXT("Font'/Game/UnifrakturCook-Bold_Font.UnifrakturCook-Bold_Font'"));
    if (FontObject.Object)
    {
        GameFont = FontObject.Object;
    }*/
    
    IsGameOver = false;
    
    InstructionText = "Use the WASD keys to move";
    BearText = "";
    TimerText = "Time Remaining: 05:00";
    DamagedMessage = "";
    ObjectsRemainingText = "Objects remaining: 0";
    ObjectsRemainingPercent = 0;
    EndGameText = "";
    
    TimeLostText = "";
    
    TimerTextColor = FColor::White;
}

void AVideoGameFPHUD::BeginPlay() {
    UpdatePaintedObjectsCount();
}

void AVideoGameFPHUD::DrawHUD()
{
	Super::DrawHUD();
    
    UpdatePaintedObjectsCount();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition( (Center.X),
										   (Center.Y + 20.0f));

	// Draw the crosshair
	FCanvasTileItem TileItem( CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem( TileItem );
    
    // Draw Instruction text
    float textWidth, textHeight;
    GetTextSize(InstructionText, textWidth, textHeight, NULL, 2.0);
    DrawText(InstructionText, FColor::White, Center.X-textWidth/2, Center.Y-50, NULL, 2.0, false);
    
 
    
    DrawText(BearText, FColor::Yellow, Center.X - 200.0, Center.Y - 100);
    
    // Timer text
    float timerTextWidth, timerTextHeight;
    GetTextSize(TimerText, timerTextWidth, timerTextHeight, NULL, 2.0);
    DrawText(TimerText, TimerTextColor, Center.X-timerTextWidth/2, Center.Y-300, NULL, 2.0, false);
    
    // Time lost text
    DrawText(TimeLostText, FColor::Red, Center.X+timerTextWidth/2+10, Center.Y-305, NULL, 2.5, false);
    
    // Damage taken text
    float DamageTextWidth, DamageTextHeight;
    GetTextSize(DamagedMessage, DamageTextWidth, DamageTextHeight, NULL, 2.0);
    DrawText(DamagedMessage, FColor::Red, Center.X-DamageTextWidth/2, Center.Y+50, NULL, 2.0, false);
    
    
    DrawText(ObjectsRemainingText, FColor::Yellow, 40, 50, NULL, 2.0, false);
    DrawRect(FColor::Black, 38, 38, 304, 14);
    DrawRect(FColor::Red, 40, 40, 300*ObjectsRemainingPercent, 10);
    
    DrawRect(FColor::Black, Center.X-1104/2, 668, 1104, 14);
    DrawRect(FColor::White, Center.X-1100*TimerPercent/2, 670, 1100*TimerPercent, 10);
    
    if (FrozenPercent > 0) {
        DrawRect(FColor::Black, Center.X-1104/2, 644, 1104, 14);
        DrawRect(FColor::Blue, Center.X-1100*FrozenPercent/2, 646, 1100*FrozenPercent, 10);
    }
    
    // End of game text
    float EndGameTextWidth, EndGameTextHeight;
    GetTextSize(EndGameText, EndGameTextWidth, EndGameTextHeight, NULL, 2.5);
    DrawText(EndGameText, FColor::White, Center.X-EndGameTextWidth/2, Center.Y, NULL, 2.5, false);
}

void AVideoGameFPHUD::SetInstructionText(FString SomeText) {
    
    InstructionText = SomeText;
}

void AVideoGameFPHUD::SetDamagedMessage(FString damageText) {
    DamagedMessage = damageText;
}

void AVideoGameFPHUD::ClearDamagedMessage() {
    DamagedMessage = "";
}

void AVideoGameFPHUD::SetEndGameText(FString text) {
    EndGameText = text;
}

void AVideoGameFPHUD::ToggleBearText() {
    if (BearText == "") {
        BearText = "Oh no, there's a bear! Maybe you should use your bullets to tranquilize him...";
    } else {
        BearText = "";
    }
}

void AVideoGameFPHUD::HandleGameOver() {
    GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
}


void AVideoGameFPHUD::DisplayTimeLost() {
    TimerTextColor = FColor::Red;
    TimeLostText = "-20s";
    
    FTimerHandle TempTimer;
    GetWorldTimerManager().SetTimer(TempTimer, this, &AVideoGameFPHUD::HideTimeLost, 2.0f);
}

void AVideoGameFPHUD::HideTimeLost() {
    TimerTextColor = FColor::White;
    TimeLostText = "";
}

void AVideoGameFPHUD::TimerOver() {
    FTimerHandle TempTimer;
    SetEndGameText("Time is over, you lost!!");
    GetWorldTimerManager().SetTimer(TempTimer, this, &AVideoGameFPHUD::HandleGameOver, 6.0f);
}



void AVideoGameFPHUD::UpdatePaintedObjectsCount() {
    if (GetWorld()) {
        AVideoGameFPGameMode* gameMode = Cast<AVideoGameFPGameMode>(GetWorld()->GetAuthGameMode());
        int numObjects = gameMode->GetPaintableObjectsCount();
        
        if (numObjects <= 0) {
            if (!IsGameOver) {
                SetEndGameText("Congratulations! You won!!!");
                FTimerHandle TempTimer;
                GetWorldTimerManager().SetTimer(TempTimer, this, &AVideoGameFPHUD::HandleGameOver, 6.0f);
            } else {
                IsGameOver = true;
            }
            
        }
        
        FString countString = FString(TEXT("Remaining objects: "));
        countString.AppendInt(numObjects);
        ObjectsRemainingText = countString;
        ObjectsRemainingPercent = gameMode->GetPaintableObjectsPercent();
    }
}

