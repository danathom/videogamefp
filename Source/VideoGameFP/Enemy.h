// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "VideoGameFPHUD.h"
#include "Enemy.generated.h"

UCLASS()
class VIDEOGAMEFP_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();
    bool frozen = false;
    
    // Called every frame
    virtual void Tick(float DeltaTime) override;
    
    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
    
    void StartAttack();
    void StopAttack();
    void StartChase();
    void StopChase();
    void DamagePlayer();
    void startBearWarningDissappearTimer();
    void ToggleBearText();
    void OnHit();
    void Unfreeze();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Assets")
    USkeletalMesh* FreezeMeshAsset;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Assets")
    USkeletalMesh* UnfreezeMeshAsset;

private:
    float range = 100.0f;
    float MAX_WALK_SPEED = 600.0f;
	FTimerHandle AttackTimer;
    FTimerHandle bearWarningTimer;
    FTimerHandle ClearBearAttack;
    AVideoGameFPHUD *HUD;
    FTimerHandle Timer;
    bool firstAttack = true;
    
    UPROPERTY(EditDefaultsOnly, Category = Animation)
    class UAnimMontage* AttackAnim;
    
    UPROPERTY(EditDefaultsOnly, Category = Animation)
    class UAnimMontage* ChaseAnim;
    
    void ClearBearTimerText();
    float frozenSecondsLeft;
};
