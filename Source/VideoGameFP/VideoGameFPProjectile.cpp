// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "VideoGameFP.h"
#include "VideoGameFPProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "PaintableObjets/PaintableObject.h"
#include "PaintableObjets/PaintableObject1.h"
#include "PaintableObjets/PaintableObject2.h"
#include "PaintableObjets/PaintableObject3.h"
#include "PaintableObjets/PaintableObject4.h"
#include "PaintableObjets/PaintableObject5.h"
#include "PaintableObjets/PaintableObject6.h"
#include "PaintableObjets/PaintableObject7.h"
#include "PaintableObjets/PaintableObject8.h"
#include "PaintableObjets/PaintableObject9.h"
#include "PaintableObjets/PaintableObject10.h"
#include "PaintableObjets/PaintableObject11.h"
#include "PaintableObjets/PaintableObject12.h"
#include "PaintableObjets/PaintableObject13.h"
#include "PaintableObjets/PaintableObject14.h"
#include "Enemy.h"

AVideoGameFPProjectile::AVideoGameFPProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &AVideoGameFPProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;
    
    AudioComp = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComp"));
    AudioComp->bAutoActivate = false;
    AudioComp->bAutoDestroy = false;
}

void AVideoGameFPProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
    
	// Only add impulse and destroy projectile if we hit a physics
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && OtherComp->IsSimulatingPhysics())
	{
		OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());

		Destroy();
	}
    
    if ((OtherActor != NULL)) {
        
        APaintableObject* PaintObject = Cast<APaintableObject>(OtherActor);
        if (PaintObject) {
            PaintObject->OnHit();
            Destroy();
        }
        
        APaintableObject1* PaintObject1 = Cast<APaintableObject1>(OtherActor);
        if (PaintObject1) {
            PaintObject1->OnHit();
            Destroy();
        }
        
        APaintableObject2* PaintObject2 = Cast<APaintableObject2>(OtherActor);
        if (PaintObject2) {
            PaintObject2->OnHit();
            Destroy();
        }
        
        APaintableObject3* PaintObject3 = Cast<APaintableObject3>(OtherActor);
        if (PaintObject3) {
            PaintObject3->OnHit();
            Destroy();
        }
        APaintableObject4* PaintObject4 = Cast<APaintableObject4>(OtherActor);
        if (PaintObject4) {
            PaintObject4->OnHit();
            Destroy();
        }
        APaintableObject5* PaintObject5 = Cast<APaintableObject5>(OtherActor);
        if (PaintObject5) {
            PaintObject5->OnHit();
            Destroy();
        }
        APaintableObject6* PaintObject6 = Cast<APaintableObject6>(OtherActor);
        if (PaintObject6) {
            PaintObject6->OnHit();
            Destroy();
        }
        APaintableObject7* PaintObject7 = Cast<APaintableObject7>(OtherActor);
        if (PaintObject7) {
            PaintObject7->OnHit();
            Destroy();
        }
        APaintableObject8* PaintObject8 = Cast<APaintableObject8>(OtherActor);
        if (PaintObject8) {
            PaintObject8->OnHit();
            Destroy();
        }
        APaintableObject9* PaintObject9 = Cast<APaintableObject9>(OtherActor);
        if (PaintObject9) {
            PaintObject9->OnHit();
            Destroy();
        }
        APaintableObject10* PaintObject10 = Cast<APaintableObject10>(OtherActor);
        if (PaintObject10) {
            PaintObject10->OnHit();
            Destroy();
        }
        APaintableObject11* PaintObject11 = Cast<APaintableObject11>(OtherActor);
        if (PaintObject11) {
            PaintObject11->OnHit();
            Destroy();
        }
        APaintableObject12* PaintObject12 = Cast<APaintableObject12>(OtherActor);
        if (PaintObject12) {
            PaintObject12->OnHit();
            Destroy();
        }
        APaintableObject13* PaintObject13 = Cast<APaintableObject13>(OtherActor);
        if (PaintObject13) {
            PaintObject13->OnHit();
            Destroy();
        }
        APaintableObject14* PaintObject14 = Cast<APaintableObject14>(OtherActor);
        if (PaintObject14) {
            PaintObject14->OnHit();
            Destroy();
        }
        
        
        AEnemy* Enemy = Cast<AEnemy>(OtherActor);
        if (Enemy) {
            Enemy->OnHit();
            Destroy();
        }
        
        
        //Play hit noise
        if (HitSound != NULL) {
            AudioComp->SetSound((USoundBase*) HitSound);
            AudioComp->Play();
        }
    }
}
