// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "VideoGameFP.h"
#include "VideoGameFPCharacter.h"
#include "VideoGameFPProjectile.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "MotionControllerComponent.h"
#include "SpawnManager.h"
#include "EngineUtils.h"


#include <iostream>

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AVideoGameFPCharacter

AVideoGameFPCharacter::AVideoGameFPCharacter()
{
    instructionState = InstructionState::Move;
    
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->Hand = EControllerHand::Right;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;
    
    AudioComp = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComp"));
    AudioComp->bAutoActivate = false;
    AudioComp->bAutoDestroy = false;
}

void AVideoGameFPCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}
    
    HUD = Cast<AVideoGameFPHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
    GetWorldTimerManager().SetTimer(BirdChirpTimer, this, &AVideoGameFPCharacter::PlayBirdNoise, 5.0f, false);
    GetWorldTimerManager().SetTimer(GameTimer, this, &AVideoGameFPCharacter::OnTimerTick, 1.0f, true);
}

//////////////////////////////////////////////////////////////////////////
// Input

void AVideoGameFPCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
    
    PlayerInputComponent->BindAction("Restart", IE_Released, this, &AVideoGameFPCharacter::RestartGame);
    PlayerInputComponent->BindAction("EndGame", IE_Released, this, &AVideoGameFPCharacter::EndGame);

	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AVideoGameFPCharacter::TouchStarted);
	if (EnableTouchscreenMovement(PlayerInputComponent) == false)
	{
		PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AVideoGameFPCharacter::OnFire);
	}

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AVideoGameFPCharacter::OnResetVR);

	PlayerInputComponent->BindAxis("MoveForward", this, &AVideoGameFPCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AVideoGameFPCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AVideoGameFPCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AVideoGameFPCharacter::LookUpAtRate);
}

void AVideoGameFPCharacter::OnFire()
{
    
    // change instruction text
    if (instructionState == InstructionState::Shoot) {
        HUD->SetInstructionText("Shoot red objects to color them");
        instructionState = InstructionState::ShootObject;
        FTimerHandle TempTimer;
        GetWorldTimerManager().SetTimer(TempTimer, this, &AVideoGameFPCharacter::ClearInstructionText, 2.0f);
    }
    
	// try and fire a projectile
	if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			if (bUsingMotionControllers)
			{
				const FRotator SpawnRotation = VR_MuzzleLocation->GetComponentRotation();
				const FVector SpawnLocation = VR_MuzzleLocation->GetComponentLocation();
				World->SpawnActor<AVideoGameFPProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
			}
			else
			{
				const FRotator SpawnRotation = GetControlRotation();
				// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
				const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

				//Set Spawn Collision Handling Override
				FActorSpawnParameters ActorSpawnParams;
				ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

				// spawn the projectile at the muzzle
				World->SpawnActor<AVideoGameFPProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
			}
		}
	}

	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	// try and play a firing animation if specified
	if (FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

void AVideoGameFPCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AVideoGameFPCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AVideoGameFPCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = false;
}

//Commenting this section out to be consistent with FPS BP template.
//This allows the user to turn without using the right virtual joystick

//void AVideoGameFPCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
//{
//	if ((TouchItem.bIsPressed == true) && (TouchItem.FingerIndex == FingerIndex))
//	{
//		if (TouchItem.bIsPressed)
//		{
//			if (GetWorld() != nullptr)
//			{
//				UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
//				if (ViewportClient != nullptr)
//				{
//					FVector MoveDelta = Location - TouchItem.Location;
//					FVector2D ScreenSize;
//					ViewportClient->GetViewportSize(ScreenSize);
//					FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
//					if (FMath::Abs(ScaledDelta.X) >= 4.0 / ScreenSize.X)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.X * BaseTurnRate;
//						AddControllerYawInput(Value);
//					}
//					if (FMath::Abs(ScaledDelta.Y) >= 4.0 / ScreenSize.Y)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.Y * BaseTurnRate;
//						AddControllerPitchInput(Value);
//					}
//					TouchItem.Location = Location;
//				}
//				TouchItem.Location = Location;
//			}
//		}
//	}
//}

void AVideoGameFPCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
        // change instruction text
        if (instructionState == InstructionState::Move) {
            HUD->SetInstructionText("Click left mouse button to shoot");
            instructionState = InstructionState::Shoot;
        }
        
        if (footStepsCounter >= 7) {
            this->PlayMovementNoise();
            footStepsCounter = 0;
        }
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AVideoGameFPCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
        // change instruction text
        if (instructionState == InstructionState::Move) {
            HUD->SetInstructionText("Click left mouse button to shoot");
            instructionState = InstructionState::Shoot;
        }
        if (footStepsCounter >= 7) {
            this->PlayMovementNoise();
            footStepsCounter = 0;
        }
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AVideoGameFPCharacter::PlayMovementNoise() {
    //Play footsteps
    if (FootstepsSound != NULL) {
        AudioComp->SetSound((USoundBase*) FootstepsSound);
        AudioComp->Play();
        footStepsCounter ++;
    }
}

void AVideoGameFPCharacter::PlayBirdNoise() {
    //Play sound of birds chirping
    if (ChirpingSound != NULL) {
        AudioComp->SetSound((USoundBase*) ChirpingSound);
        AudioComp->Play();
    }
    GetWorldTimerManager().SetTimer(BirdChirpTimer, this, &AVideoGameFPCharacter::PlayBirdNoise, 30.0f, false);
}

void AVideoGameFPCharacter::PlayRestartNoise() {
    //Play sound of birds chirping
    if (RestartSound != NULL) {
        AudioComp->SetSound((USoundBase*) RestartSound);
        AudioComp->Play();
    }
}

void AVideoGameFPCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AVideoGameFPCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool AVideoGameFPCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	bool bResult = false;
	if (FPlatformMisc::GetUseVirtualJoysticks() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		bResult = true;
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AVideoGameFPCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &AVideoGameFPCharacter::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AVideoGameFPCharacter::TouchUpdate);
	}
	return bResult;
}

void AVideoGameFPCharacter::RestartGame() {
    std::cout << "Restarting game" << std::endl;
    UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
    this->PlayRestartNoise();
}

void AVideoGameFPCharacter::EndGame() {
    std::cout << "Ending Game" << std::endl;
    GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
}

void AVideoGameFPCharacter::ClearInstructionText() {
    if (instructionState == InstructionState::ShootObject) {
        HUD->SetInstructionText("Color all red objects before time runs out!");
        instructionState = InstructionState::Playing;
        FTimerHandle TempTimer;
        GetWorldTimerManager().SetTimer(TempTimer, this, &AVideoGameFPCharacter::ClearInstructionText, 6.0f);
    } else {
        HUD->SetInstructionText("");
        
        for (TActorIterator<ASpawnManager> ActorItr(GetWorld()); ActorItr; ++ActorItr)
        {
            // Same as with the Object Iterator, access the subclass instance with the * or -> operators.
            ASpawnManager *spawnManager = *ActorItr;
            spawnManager->SpawnBear();
        }
    }
}

void AVideoGameFPCharacter::OnTimerTick()
{
    if (secondsLeft <= 1) {
        GetWorldTimerManager().ClearTimer(GameTimer);
        HUD->TimerOver();
//        GetWorldTimerManager().SetTimer(EndGameTimer, this, &AVideoGameFPCharacter::EndGameOnTimerRunout, 1.0f, false);
    }
    secondsLeft--;
    const FString TimeDesc = FString::Printf(TEXT("Time Remaining: %02d:%02d"), secondsLeft/60, secondsLeft%60);
    HUD->SetTimerText(TimeDesc);
    HUD->SetTimerPercent((float)secondsLeft/300.0);
}

void AVideoGameFPCharacter::EndGameOnTimerRunout() {
    GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
}


