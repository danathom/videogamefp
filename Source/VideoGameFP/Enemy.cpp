// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "EnemyAIController.h"
#include "VideoGameFPGameMode.h"
#include "VideoGameFPCharacter.h"
#include "Enemy.h"

#include <iostream>

// Sets default values
AEnemy::AEnemy()
{
    PrimaryActorTick.bCanEverTick = true;
    AIControllerClass = AEnemyAIController::StaticClass();
    frozenSecondsLeft = 0;
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
    Super::BeginPlay();
    APawn* player = UGameplayStatics::GetPlayerPawn(this, 0);
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    if (frozenSecondsLeft > 0) {
        frozenSecondsLeft -= DeltaTime;
        HUD = Cast<AVideoGameFPHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
        HUD->SetBearFrozenPercent((float)frozenSecondsLeft/10.0f);
    }
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
    
}

void AEnemy::StartAttack()
{
    if (AttackAnim == NULL) {return;}
    float animDuration = PlayAnimMontage(AttackAnim);
    if (animDuration > 0) {
        DamagePlayer();
        GetWorldTimerManager().SetTimer(AttackTimer, this, &AEnemy::DamagePlayer, 0.1f, false);
    }
}

void AEnemy::StartChase()
{
    if (ChaseAnim == NULL) {return;}
    float animDuration = PlayAnimMontage(ChaseAnim);
}

void AEnemy::DamagePlayer()
{
    ((AVideoGameFPCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(),0))->secondsLeft -= 20;
    
    HUD = Cast<AVideoGameFPHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
    HUD->DisplayTimeLost();
    
    if (firstAttack) {
        HUD = Cast<AVideoGameFPHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
        HUD->SetDamagedMessage(FString("Oh no! Bear attacked you. You lost 20 seconds of time!"));
        GetWorldTimerManager().SetTimer(ClearBearAttack, this, &AEnemy::ClearBearTimerText, 5.0f, false);
        firstAttack = false;
    }
    GetWorldTimerManager().SetTimer(AttackTimer, this, &AEnemy::DamagePlayer, 3.0f, true);

}

void AEnemy::ClearBearTimerText()
{
    HUD = Cast<AVideoGameFPHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
    HUD->ClearDamagedMessage();
}

void AEnemy::StopChase()
{
    StopAnimMontage(ChaseAnim);
}

void AEnemy::StopAttack()
{
    GetWorldTimerManager().ClearTimer(AttackTimer);
    StopAnimMontage(AttackAnim);
}

void AEnemy::OnHit()
{
    GetMesh()->SetSkeletalMesh(FreezeMeshAsset);
    GetCharacterMovement()->MaxWalkSpeed = 0;
    
    frozenSecondsLeft = 10.0f;
    GetWorldTimerManager().SetTimer(Timer, this, &AEnemy::Unfreeze, 10.0f, false);
    frozen = true;
}

void AEnemy::Unfreeze()
{
    GetMesh()->SetSkeletalMesh(UnfreezeMeshAsset);
    HUD = Cast<AVideoGameFPHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
    HUD->SetBearFrozenPercent(0);
    
    GetCharacterMovement()->MaxWalkSpeed = MAX_WALK_SPEED;
}

void AEnemy::ToggleBearText()
{
    HUD = Cast<AVideoGameFPHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
    HUD->ToggleBearText();
}


void AEnemy::startBearWarningDissappearTimer()
{
    GetWorldTimerManager().SetTimer(bearWarningTimer, this, &AEnemy::ToggleBearText, 5.0f, false);
}

