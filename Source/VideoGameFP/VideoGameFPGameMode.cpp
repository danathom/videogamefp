// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "VideoGameFP.h"
#include "VideoGameFPGameMode.h"
#include "VideoGameFPHUD.h"
#include "VideoGameFPCharacter.h"
#include "PaintableObjets/Paintable.h"

AVideoGameFPGameMode::AVideoGameFPGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AVideoGameFPHUD::StaticClass();
    level = 1;
    
    maxcount = 0;
}

int AVideoGameFPGameMode::GetPaintableObjectsCount() {
    TArray<AActor*> FoundActors;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), APaintable::StaticClass(), FoundActors);
    
    int count = 0;
    for (AActor* actor : FoundActors) {
        APaintable* PaintObject = Cast<APaintable>(actor);
        
        if (PaintObject) {
            if (!PaintObject->isPainted) {
                count++;
            }
        }
    
        
    }
    objectcount = count;
    
    if (maxcount < count) {
        maxcount = count;
    }
    
    return count;
}

float AVideoGameFPGameMode::GetPaintableObjectsPercent() {
    return (float)(GetPaintableObjectsCount())/(float)(maxcount);
}
