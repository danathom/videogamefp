// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "PaintableObject7.h"


// Sets default values
APaintableObject7::APaintableObject7()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    Object7Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Object7Mesh"));
    RootComponent = Object7Mesh;
    
    static ConstructorHelpers::FObjectFinder<UMaterial> Mat(TEXT("Material'/Game/InfinityBladeGrassLands/Environments/Misc/Exo_Deco02/Materials/M_Villa_Pillar_1.M_Villa_Pillar_1'"));
    if (Mat.Succeeded()) {
        Material = (UMaterial*)Mat.Object;
    }
    isPainted = false;
    
}

// Called when the game starts or when spawned
void APaintableObject7::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void APaintableObject7::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
}

void APaintableObject7::OnHit()
{
    
    if (Material) {
        Object7Mesh->SetMaterial(0,Material);
        isPainted = true;
    }
    
}

