// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "PaintableObject14.h"


// Sets default values
APaintableObject14::APaintableObject14()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    Object14Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Object14Mesh"));
    RootComponent = Object14Mesh;
    
    static ConstructorHelpers::FObjectFinder<UMaterial> Mat(TEXT("Material'/Game/InfinityBladeGrassLands/Environments/Misc/Exo_Deco02/Materials/M_Pew.M_Pew'"));
    
    if (Mat.Succeeded()) {
        Material = (UMaterial*)Mat.Object;
    }
    isPainted = false;
}

// Called when the game starts or when spawned
void APaintableObject14::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void APaintableObject14::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
}

void APaintableObject14::OnHit()
{
    
    if (Material) {
        Object14Mesh->SetMaterial(0,Material);
        isPainted = true;
    }
}

