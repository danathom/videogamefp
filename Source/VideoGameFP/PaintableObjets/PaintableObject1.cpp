// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "PaintableObject1.h"


// Sets default values
APaintableObject1::APaintableObject1()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    Object1Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Object1Mesh"));
    RootComponent = Object1Mesh;
    
    
    //PO Leafless Tree

    static ConstructorHelpers::FObjectFinder<UMaterial> Mat(TEXT("Material'/Game/InfinityBladeGrassLands/Environments/Plains/Env_Plains_Flora/Materials/M_Plains_Tree_Stump01.M_Plains_Tree_Stump01'"));
    if (Mat.Succeeded()) {
        Material = (UMaterial*)Mat.Object;
    }
    isPainted = false;
}

// Called when the game starts or when spawned
void APaintableObject1::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void APaintableObject1::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
}

void APaintableObject1::OnHit()
{
    
    if (Material) {
        Object1Mesh->SetMaterial(0,Material);
        isPainted = true;
    }
    
}

