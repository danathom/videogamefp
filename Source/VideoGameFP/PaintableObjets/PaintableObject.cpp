// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "PaintableObject.h"


// Sets default values
APaintableObject::APaintableObject()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    ObjectMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ObjectMesh"));
    RootComponent = ObjectMesh;
    
    
    //PO Cherry Tree
    
    static ConstructorHelpers::FObjectFinder<UMaterial> BarkMat(TEXT("Material'/Game/InfinityBladeGrassLands/Environments/Earth/Materials/M_TreeBark.M_TreeBark'"));
    
    if (BarkMat.Succeeded()) {
        BarkMaterial = (UMaterial*)BarkMat.Object;
    }
    static ConstructorHelpers::FObjectFinder<UMaterial> TreeMat(TEXT("Material'/Game/InfinityBladeGrassLands/Environments/Earth/Materials/M_Cherry_Tree.M_Cherry_Tree'"));
    
    if (TreeMat.Succeeded()) {
        TreeMaterial = (UMaterial*)TreeMat.Object;
    }
    
    isPainted = false;

}

// Called when the game starts or when spawned
void APaintableObject::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void APaintableObject::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
}

void APaintableObject::OnHit()
{
    
    if (BarkMaterial) {
        ObjectMesh->SetMaterial(0, BarkMaterial);
        isPainted = true;
    }
    
    if (TreeMaterial) {
        ObjectMesh->SetMaterial(1, TreeMaterial);
        isPainted = true;
    }

    
}

