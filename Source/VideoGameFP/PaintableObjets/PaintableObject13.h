// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Paintable.h"
#include "PaintableObject13.generated.h"

UCLASS()
class VIDEOGAMEFP_API APaintableObject13 : public APaintable
{
    GENERATED_BODY()
    
public:
    // Sets default values for this actor's properties
    APaintableObject13();
    void OnHit();
    
protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Paintable)
    UStaticMeshComponent* Object13Mesh;
    
    UMaterial* Material0;
    UMaterial* Material1;
    
    
public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;
    
    
    
};
