// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Paintable.h"
#include "PaintableObject.generated.h"

UCLASS()
class VIDEOGAMEFP_API APaintableObject : public APaintable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APaintableObject();
    void OnHit();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
    
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Paintable)
    UStaticMeshComponent* ObjectMesh;
    
    UMaterial* TreeMaterial;
    UMaterial* BarkMaterial;


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
