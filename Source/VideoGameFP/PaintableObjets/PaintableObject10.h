// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Paintable.h"
#include "PaintableObject10.generated.h"

UCLASS()
class VIDEOGAMEFP_API APaintableObject10 : public APaintable
{
    GENERATED_BODY()
    
public:
    // Sets default values for this actor's properties
    APaintableObject10();
    void OnHit();
    
protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Paintable)
    UStaticMeshComponent* Object10Mesh;
    
    UMaterial* Material;
    
    
public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;
    
    
    
};
