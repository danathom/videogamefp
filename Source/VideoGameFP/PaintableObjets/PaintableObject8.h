// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Paintable.h"
#include "PaintableObject8.generated.h"

UCLASS()
class VIDEOGAMEFP_API APaintableObject8 : public APaintable
{
    GENERATED_BODY()
    
public:
    // Sets default values for this actor's properties
    APaintableObject8();
    void OnHit();
protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Paintable)
    UStaticMeshComponent* Object8Mesh;
    
    UMaterial* Material;
    
public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;
    
    
    
};
