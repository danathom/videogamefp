// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Paintable.h"
#include "PaintableObject4.generated.h"

UCLASS()
class VIDEOGAMEFP_API APaintableObject4 : public APaintable
{
    GENERATED_BODY()
    
public:
    // Sets default values for this actor's properties
    APaintableObject4();
    void OnHit();    
protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Paintable)
    UStaticMeshComponent* Object4Mesh;
    
    UMaterial* Material0;
    UMaterial* Material1;
    UMaterial* Material2;
    UMaterial* Material3;
    UMaterial* Material4;
    
    
public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;
    
    
    
};
