// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "Paintable.h"


// Sets default values
APaintable::APaintable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APaintable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APaintable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

