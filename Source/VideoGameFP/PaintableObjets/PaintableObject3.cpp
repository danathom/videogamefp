// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "PaintableObject3.h"


// Sets default values
APaintableObject3::APaintableObject3()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    Object3Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Object3Mesh"));
    RootComponent = Object3Mesh;
    
    
    //PO Arc
    
    static ConstructorHelpers::FObjectFinder<UMaterial> Mat(TEXT("Material'/Game/InfinityBladeGrassLands/Environments/Misc/Exo_Deco02/Materials/M_Villa_Pillar_1.M_Villa_Pillar_1'"));
    if (Mat.Succeeded()) {
        Material = (UMaterial*)Mat.Object;
    }
    isPainted = false;
    
}

// Called when the game starts or when spawned
void APaintableObject3::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void APaintableObject3::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
}

void APaintableObject3::OnHit()
{
    
    if (Material) {
        Object3Mesh->SetMaterial(0,Material);
        isPainted = true;
    }
    
}

