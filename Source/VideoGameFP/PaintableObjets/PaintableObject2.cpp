// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "PaintableObject2.h"


// Sets default values
APaintableObject2::APaintableObject2()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    Object2Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Object2Mesh"));
    RootComponent = Object2Mesh;
    
    
    //PO Angel Statue
    
    static ConstructorHelpers::FObjectFinder<UMaterial> Mat(TEXT("Material'/Game/InfinityBladeGrassLands/Environments/Plains/Env_Plains_Statues/Materials/M_Plains_Angel_Statue.M_Plains_Angel_Statue'"));
    if (Mat.Succeeded()) {
        Material = (UMaterial*)Mat.Object;
    }
    isPainted = false;
    
}

// Called when the game starts or when spawned
void APaintableObject2::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void APaintableObject2::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
}

void APaintableObject2::OnHit()
{
    
    if (Material) {
        Object2Mesh->SetMaterial(0,Material);
        isPainted = true;
    }
    
}

