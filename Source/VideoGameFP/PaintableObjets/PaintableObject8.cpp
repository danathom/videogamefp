// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "PaintableObject8.h"


// Sets default values
APaintableObject8::APaintableObject8()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    Object8Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Object8Mesh"));
    RootComponent = Object8Mesh;
    
    static ConstructorHelpers::FObjectFinder<UMaterial> Mat(TEXT("Material'/Game/InfinityBladeGrassLands/Environments/Misc/Exo_Deco02/Materials/M_HardWood_Floor.M_HardWood_Floor'"));

    if (Mat.Succeeded()) {
        Material = (UMaterial*)Mat.Object;
    }
    isPainted = false;
    
}

// Called when the game starts or when spawned
void APaintableObject8::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void APaintableObject8::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
}

void APaintableObject8::OnHit()
{
    
    if (Material) {
        Object8Mesh->SetMaterial(0,Material);
        isPainted = true;
    }
    
}

