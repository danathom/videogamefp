// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "PaintableObject10.h"


// Sets default values
APaintableObject10::APaintableObject10()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    Object10Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Object10Mesh"));
    RootComponent = Object10Mesh;
    
    
    //PO Leafless Tree
    
    static ConstructorHelpers::FObjectFinder<UMaterial> Mat(TEXT("Material'/Game/InfinityBladeGrassLands/Environments/Plains/Env_Plains_Ruins/Materials/M_Plains_Pillars02.M_Plains_Pillars02'"));
    if (Mat.Succeeded()) {
        Material = (UMaterial*)Mat.Object;
    }
    isPainted = false;
}

// Called when the game starts or when spawned
void APaintableObject10::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void APaintableObject10::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
}

void APaintableObject10::OnHit()
{
    
    if (Material) {
        Object10Mesh->SetMaterial(0,Material);
        isPainted = true;
    }
    
}

