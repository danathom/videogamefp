// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "PaintableObject11.h"


// Sets default values
APaintableObject11::APaintableObject11()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    Object11Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Object11Mesh"));
    RootComponent = Object11Mesh;
    
    
    //PO Leafless Tree
    
    static ConstructorHelpers::FObjectFinder<UMaterial> Mat(TEXT("Material'/Game/InfinityBladeGrassLands/Environments/Plains/Env_Plains_Ruins/Materials/M_Plains_Pillars02.M_Plains_Pillars02'"));
    if (Mat.Succeeded()) {
        Material = (UMaterial*)Mat.Object;
    }
    isPainted = false;
}

// Called when the game starts or when spawned
void APaintableObject11::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void APaintableObject11::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
}

void APaintableObject11::OnHit()
{
    
    if (Material) {
        Object11Mesh->SetMaterial(0,Material);
        isPainted = true;
    }
    
}

