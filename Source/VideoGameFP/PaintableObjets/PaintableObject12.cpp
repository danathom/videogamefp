// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "PaintableObject12.h"


// Sets default values
APaintableObject12::APaintableObject12()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    Object12Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Object12Mesh"));
    RootComponent = Object12Mesh;
    
    static ConstructorHelpers::FObjectFinder<UMaterial> Mat0(TEXT("Material'/Game/InfinityBladeGrassLands/Environments/Plains/Env_Plains_Ruins/Materials/M_Plains_Pillars03.M_Plains_Pillars03'"));
    
    if (Mat0.Succeeded()) {
        Material0 = (UMaterial*)Mat0.Object;
    }
    static ConstructorHelpers::FObjectFinder<UMaterial> Mat1(TEXT("Material'/Game/InfinityBladeGrassLands/Environments/Plains/Env_Plains_Ruins/Materials/M_Plains_StoneWall.M_Plains_StoneWall'"));
    
    if (Mat1.Succeeded()) {
        Material1 = (UMaterial*)Mat1.Object;
    }
    isPainted = false;
}

// Called when the game starts or when spawned
void APaintableObject12::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void APaintableObject12::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
}

void APaintableObject12::OnHit()
{
    
    if (Material0) {
        Object12Mesh->SetMaterial(0,Material0);
        isPainted = true;
    }
    if (Material1) {
        Object12Mesh->SetMaterial(1,Material1);
        isPainted = true;
    }
    
}

