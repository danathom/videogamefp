// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "PaintableObject6.h"


// Sets default values
APaintableObject6::APaintableObject6()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    Object6Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Object6Mesh"));
    RootComponent = Object6Mesh;
 
    static ConstructorHelpers::FObjectFinder<UMaterial> Mat(TEXT("Material'/Game/InfinityBladeGrassLands/Environments/Breakables/Materials/M_Breakables_Barrels.M_Breakables_Barrels'"));
    if (Mat.Succeeded()) {
        Material = (UMaterial*)Mat.Object;
    }
    isPainted = false;
    
}

// Called when the game starts or when spawned
void APaintableObject6::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void APaintableObject6::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
}

void APaintableObject6::OnHit()
{
    
    if (Material) {
        Object6Mesh->SetMaterial(0,Material);
        isPainted = true;
    }
    
}

