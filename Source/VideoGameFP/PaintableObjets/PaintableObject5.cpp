// Fill out your copyright notice in the Description page of Project Settings.

#include "VideoGameFP.h"
#include "PaintableObject5.h"


// Sets default values
APaintableObject5::APaintableObject5()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    Object5Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Object5Mesh"));
    RootComponent = Object5Mesh;
 
    static ConstructorHelpers::FObjectFinder<UMaterial> Mat0(TEXT("Material'/Game/KiteDemo/Environments/Trees/ScotsPine_01/ScotsPine_01_Branches_Mat'"));
    if (Mat0.Succeeded()) {
        Material0 = (UMaterial*)Mat0.Object;
    }
    static ConstructorHelpers::FObjectFinder<UMaterial> Mat1(TEXT("Material'/Game/KiteDemo/Environments/Trees/ScotsPine_01/ScotsPine_01_Branches_2_Mat'"));
    if (Mat1.Succeeded()) {
        Material1 = (UMaterial*)Mat1.Object;
    }
    static ConstructorHelpers::FObjectFinder<UMaterial> Mat2(TEXT("Material'/Game/KiteDemo/Environments/Trees/ScotsPine_01/ScotsPine_01_Leaves_Mat'"));
    if (Mat2.Succeeded()) {
        Material2 = (UMaterial*)Mat2.Object;
    }
    static ConstructorHelpers::FObjectFinder<UMaterial> Mat3(TEXT("Material'/Game/KiteDemo/Environments/Trees/ScotsPine_01/ScotsPine_01_Fronds_Mat'"));
    if (Mat3.Succeeded()) {
        Material3 = (UMaterial*)Mat3.Object;
    }
    static ConstructorHelpers::FObjectFinder<UMaterial> Mat4(TEXT("Material'/Game/KiteDemo/Environments/Trees/ScotsPineTall_01/ScotsPineTall_01_Billboard_Mat.ScotsPineTall_01_Billboard_Mat'"));
    if (Mat4.Succeeded()) {
        Material4 = (UMaterial*)Mat4.Object;
    }
    isPainted = false;
    
}

// Called when the game starts or when spawned
void APaintableObject5::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void APaintableObject5::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
}

void APaintableObject5::OnHit()
{
    
    if (Material0) {
        Object5Mesh->SetMaterial(0,Material0);
        isPainted = true;
    }
    if (Material1) {
        Object5Mesh->SetMaterial(1,Material1);
        isPainted = true;
    }
    if (Material2) {
        Object5Mesh->SetMaterial(2,Material2);
        isPainted = true;
    }
    if (Material3) {
        Object5Mesh->SetMaterial(3,Material3);
        isPainted = true;
    }
    if (Material4) {
        Object5Mesh->SetMaterial(4,Material4);
        isPainted = true;
    }
    
}

