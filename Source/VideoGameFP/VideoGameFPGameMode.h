// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "VideoGameFPHUD.h"
#include "GameFramework/GameModeBase.h"
#include "VideoGameFPGameMode.generated.h"

UCLASS(minimalapi)
class AVideoGameFPGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AVideoGameFPGameMode();
    
    int GetPaintableObjectsCount();
    float GetPaintableObjectsPercent();
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=StatusBar)
    int objectcount;
    
private:
    int level;
    int maxcount;
};



